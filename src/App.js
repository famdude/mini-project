import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import {Route, Link, BrowserRouter as Router} from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Avatar from '@material-ui/core/Avatar';
import LockIcon from '@material-ui/icons/Lock';
import {pink, blue} from '@material-ui/core/colors';
import Grid from '@material-ui/core/Grid';



const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  pink: {
    color: theme.palette.getContrastText(pink[500]),
    backgroundColor: pink[500],
  },
  blue: {
    color: theme.palette.getContrastText(blue[500]),
    backgroundColor: blue[500],
  },
}));

export function Signin() {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    password1: '',
    password2: '',
    showPassword: false,
    message: '',
    
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };
  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const handleValidation =  () => {
    let valNumber = Math.floor(Math.random() * 10);
    if (valNumber < 5) {
      setValues({ ...values, message: "Welcome!"})
    }
    else {
      setValues({ ...values, message: "Username and password doesn't match!"})
    }
  }

  return (
    <div>
    <Avatar className={classes.pink}>
    <LockIcon />
    </Avatar>
    <h2> Sign In </h2>
    <form className={classes.root} noValidate autoComplete="off">
    <Grid container spacing={3} justify="center" alignItems="center">
    <Grid container item xs={12} justify="center" alignItems="center">
    <Grid item xs={4}>
    <TextField placeholder="Username" inputProps={{ 'aria-label': 'description' }} size="normal" 
    fullWidth={true} />
    </Grid>
    </Grid>
    <Grid container item xs={12} justify="center" alignItems="center">
    <Grid item xs={4}>
    <Input placeholder="Password" type={values.showPassword ? 'text' : 'password'} size="normal"
    fullWidth={true} value={values.password}  
    onChange={handleChange('password')} inputProps={{ 'aria-label': 'description' }}
    endAdornment={
      <InputAdornment position="end">
        <IconButton
          aria-label="toggle password visibility"
          onClick={handleClickShowPassword}
          onMouseDown={handleMouseDownPassword}
        >
          {values.showPassword ? <Visibility /> : <VisibilityOff />}
        </IconButton>
      </InputAdornment>
    } 
    />
    </Grid>
    </Grid>
    </Grid>
    <br />
    <Button variant="contained" color="primary" onClick={handleValidation}>Sign In</Button>
    <Link to="/signup"><Button variant="contained">Sign Up</Button></Link>
    <br />
    <div>{values.message}</div>
    </form>
    
    </div>
    
  );
}


export function Signup() {
    const classes = useStyles();
    const [values, setValues] = React.useState({
        password: '',
        showPassword: false,
        message: '',
        
      });
    
      const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
      };
      const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
      };
      const handleMouseDownPassword = (event) => {
        event.preventDefault();
      };
      const handleValidation =  () => {
        let valNumber = Math.floor(Math.random() * 10);
        if (valNumber < 5) {
          setValues({ ...values, message: "Welcome!"})
        }
        else {
          setValues({ ...values, message: "Username has been taken!"})
        }
      }

    return (
        <div>
            <Avatar className={classes.blue}>
            <LockIcon />
            </Avatar>
            <h2> Sign Up </h2>
            <form className={classes.root} noValidate autoComplete="off">
            <Grid container spacing={3} justify="center" alignItems="center">
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <TextField placeholder="First Name" inputProps={{ 'aria-label': 'description' }} 
            size="normal" fullWidth={true} />
            </Grid>
            </Grid>
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <TextField placeholder="Last Name" inputProps={{ 'aria-label': 'description' }} 
            size="normal" fullWidth={true} />
            </Grid>
            </Grid>
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <TextField placeholder="Email" inputProps={{ 'aria-label': 'description' }} 
            size="normal" fullWidth={true} />
            </Grid>
            </Grid>
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <TextField placeholder="Username" inputProps={{ 'aria-label': 'description' }} 
            size="normal" fullWidth={true} />
            </Grid>
            </Grid>
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <Input placeholder="Password" type={values.showPassword ? 'text' : 'password'} 
            size="normal" fullWidth={true} value={values.password1}  
            onChange={handleChange('password')} inputProps={{ 'aria-label': 'description' }}
            endAdornment={
            <InputAdornment position="end">
                <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
            </InputAdornment>
            } 
            />
            </Grid>
            </Grid>
            <Grid container item xs={6} justify="center" alignItems="center">
            <Grid item xs={8}>
            <Input placeholder="Claim Password" type={values.showPassword ? 'text' : 'password'} 
            size="normal" fullWidth={true} value={values.password2}  
            onChange={handleChange('password')} inputProps={{ 'aria-label': 'description' }}
            endAdornment={
            <InputAdornment position="end">
                <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
            </InputAdornment>
            } 
            />
            </Grid>
            </Grid>
            </Grid>
            <br />
            <Button variant="contained" color="primary" onClick={handleValidation}>Sign Up</Button>
            <Link to="/"><Button variant="contained">Sign In</Button></Link>
            <br />
            <div>{values.message}</div>
            </form>
        </div>
    );
}


class App extends Component {
    render() {
        return (
            <div>
                <Route path='/' component={Signin} exact/>
                <Route path='/signup' component={Signup} exact/>
            </div>
            
        );
    }
}

export default App;